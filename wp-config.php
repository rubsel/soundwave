<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dev_sound');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8lgG)bLkf=68S`i:LP+#o^tP.+k~+7?D_a&0w[CaWr${hwf7O{/=T|C%=Y3y|TKn');
define('SECURE_AUTH_KEY',  's(Jle+6+n+gAAr#W2LE+`cqvU_lXT.r7_Q&3fB)A_-N=;tJy1awOsB{Q-1Qc.v}r');
define('LOGGED_IN_KEY',    'e{wC3rsr>Zc=pb7u*xw^0U.s!shfbloP)=t&U9_E$DH%Z5rf=>|MjA|VR3SHX_40');
define('NONCE_KEY',        'X.tCQh66X#RZC?F> +0:4a+cs]6?t.aDh)a6S1CT_0uzl5_U|5p&]oC=)xfU>0%r');
define('AUTH_SALT',        'T#otyN|21a:QV-f7V,OOv#z40XkD9=>vy+HS|n|]&vuPGC+ADei^!v|vD{LYl+8;');
define('SECURE_AUTH_SALT', 'r[z+pHG1s*Zf&?GiUMdpH*2uDnvw#&FeR0X+rX4g&wp4eM-2X/WG]Th[OaG9zRX.');
define('LOGGED_IN_SALT',   's4./cNZ/&V}h`7l+uW+~0CXLQgqe/u>mZUOy+}wr|wR|J`mx&%Nbd:7+yRSc!5V,');
define('NONCE_SALT',       '^2/~4rKB+8B$+YowCH|rs$bsFfZ><kF+J}%Cb-aR^kliSf#|/K2e<;}0iTZqB3F)');


/*  
 * Extra WordPress settings
 */
define('AUTOMATIC_UPDATER_DISABLED', true );
define('DISALLOW_FILE_EDIT', true );

if ( !defined('PROJECT') ) 
define( 'PROJECT', 'soundwave' . '/' ); /** On localhost change to foldername, For live remove string */

define('WP_CONTENT_FOLDERNAME', 'app');
define('WP_CONTENT_DIR', ABSPATH . WP_CONTENT_FOLDERNAME);
define('WP_SITEURL', ($_SERVER['HTTPS'] ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/' . PROJECT ); /** PROJECT is needed for local & dev setup, do not remove */
define('WP_CONTENT_URL', WP_SITEURL . WP_CONTENT_FOLDERNAME);

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sw_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
