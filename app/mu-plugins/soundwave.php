<?php 

/*
 * Plugin Name: Sound Wave
 * Author: Blik vormgeving
 * Author URI: https://blikvormgeving.nl
 * Description: All the posttypes needed to get SoundWave.nu to work.
 * Text Domain: snd
 * Domain Path: /lang
 */

function load_soundwave_textdomain() { 
	load_textdomain('snd', WPMU_PLUGIN_DIR . '/lang/snd-' . get_locale() . '.mo'); 
}
add_action('init', 'load_soundwave_textdomain');

function soundwave() {
	/**
	 * @cpt Portfolio
	 */
	$args = array(
		'labels'            => array(
			'name'              => __( 'Portfolio', 'snd' ),
			'singular_name'     => __( 'Portfolio item', 'snd' ),
			'add_new'           => __( 'Add portfolio item', 'snd' ),
			'add_new_item'      => __( 'Add new portfolio item', 'snd' )
		),
		'public'            => true,
		'has_archive'       => true,
		'menu_position'     => 5,
		'menu_icon'			=> 'dashicons-portfolio',
		'rewrite'           => array(
			'slug'              => __( 'portfolio', 'snd' )
		),
		'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail'),
	); 
	register_post_type( 'snd_portfolio', $args );
/**
 * @taxonomy Project type
 */
$args = array(
	'hierarchical' => true,
    'show_ui' => true,
    'show_admin_column' => true,
	'label' => __('Project type','rby'),
	'rewrite'       => array(
		'slug'      	=> __( 'project-type', 'snd' )
	),

);
register_taxonomy('snd_project_type','snd_portfolio',$args);

}
add_action('init','soundwave' ); 

function soundwave_queries($query) {
	if (!is_admin() AND is_post_type_archive('snd_portfolio' ) AND $query->is_main_query()) {
		$query->set('posts_per_page','-1');
	}
}
add_action('pre_get_posts','soundwave_queries');




