��          |      �             !     8  5   K     �  	   �     �     �  
   �     �  	   �     �    �     �       O   $     t  	   �     �     �  
   �     �  	   �     �                            
      	                         Add new portfolio item Add portfolio item All the posttypes needed to get SoundWave.nu to work. Blik vormgeving Portfolio Portfolio item Project type Sound Wave https://blikvormgeving.nl portfolio project-type Project-Id-Version: Sound Wave
POT-Creation-Date: 2018-07-16 11:22+0200
PO-Revision-Date: 2018-07-16 11:23+0200
Last-Translator: Ruben Zwiers <ruben@rubsel.com>
Language-Team: Ruben Zwiers <ruben@rubsel.com>
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.9
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: soundwave.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Nieuw portfolio item Portfolio item toevoegen Alle bericht types die nodig zijn om de site SoundWave.nu goed te laten werken. Blik vormgeving Portfolio Portfolio item Project type Sound Wave https://blikvormgeving.nl portfolio Projectsoort 