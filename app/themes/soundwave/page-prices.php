
<?php get_header(); 
	/*
	Template Name: Prices
	*/
	__('Prices','rby');
?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'headerphoto' );?>

<div class="headerimage" style="background-image: url(<?php echo $src[0]; ?>);">
	<h1><?php the_title(); ?></h1>
</div>

<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-sm-8">
			
				<?php while ( have_posts() ) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
						<div class="entry-content">
							<?php the_content(); ?>
						</div><!-- .entry-content -->
					</article><!-- #post-## -->
			
				<?php endwhile; // end of the loop. ?>
		
			</div><!-- close .main-content-inner -->
			<aside class="col-md-4">
				<div class="pricelist">
					<p><em><?php the_field('prijslijst_info'); ?></em></p>
					<a href="<?php esc_url(the_field('upload_prijslijst')); ?>" class="btn primary"><?php _e('Download our pricelist','rby');?></a>
				</div>
			</aside>
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
<?php get_footer(); ?>
