<?php get_header(); ?>

<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-md-8 col-sm-12">

	<article id="blog-single">
		
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
				<h1>
					<?php the_title(); ?>
				</h1>
				
				<p class="meta">
					<?php _e( 'Posted on', 'rby' ); ?> <time datetime="<?php the_time( 'Y-m-d' ); ?>"><?php echo get_the_date(); ?></time>
					<?php _e( 'by', 'rby' ) ?> <?php the_author_posts_link(); ?> 
					<?php _e( 'in the category', 'rby' ) ?> <?php the_category( ', ' ) ?>
				</p>

				<div>

						<?php if ( has_post_thumbnail() ) { ?>
							<figure class="thumb">
								<?php the_post_thumbnail( 'medium' ); ?>
							</figure>

						<?php } else { ?>
							
						<?php } ?>

					<?php 
						the_content();

						// wp_link_pages( array(
						// 	'before'         => '<nav class="pages">',
						// 	'after'          => '</nav>',
						// 	'next_or_number' => 'next'
						// ) );
					?>

				</div>


				<p class="tags">
					<?php the_tags( '<span class="label">Tags</span> ', '' ); ?>
				</p>

				<p>
					<a class="back-link" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">
						<?php _e( 'Back to the overview', 'rby' ); ?>
					</a>
				</p>

			<?php endwhile; endif; ?>
			</article>

			<?php comments_template(); ?>

	</div><!-- close .main-content-inner -->


	<div class="sidebar col-md-4 col-sm-12">

		<?php // add the class "panel" below here to wrap the sidebar in Bootstrap style ;) ?>

		<div class="sidebar-padder">
			<?php if ( function_exists('dynamic_sidebar') ) dynamic_sidebar('blog'); ?>
		</div>
	</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
			</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
<?php get_footer(); ?>