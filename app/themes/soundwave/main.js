// @codekit-prepend 'assets/js/jquery.fancybox.js'
// @codekit-prepend 'assets/js/jquery.slick.js'
// @codekit-prepend 'assets/js/jquery.cookieBar.js'

jQuery(document).ready(function($){

// Theme Main JQuery functions
	$('.handle').on('click', function() {
		$('nav ul.main-nav').toggleClass('show');
		$('body').toggleClass('show');
		$('.handle').toggleClass('show');
	});

// Adding :hover & :focus states on Mobile
	window.onload = function() {
	  if(/iP(hone|ad)/.test(window.navigator.userAgent)) {
	    document.body.addEventListener('touchstart', function() {}, false);
	  }
	};

	$('.slider').slick({
		autoplay	  : true,
		autoplaySpeed : 3000,
		draggable	  : false,
		infinite	  : true,
  		fade		  : true,
  		cssEase		  : 'linear'
	});


// jQuery Cookie notice
	$('#cookie-notice').cookieBar({ 
		closeButton : '.cookie-dismiss', 
		hideOnClose: false });
	$('#cookie-notice').on('cookieBar-close', function() { 
	 	$(this).fadeOut('1s'); 
	});

// Initialize the Lightbox and add rel="gallery" to all gallery images when the gallery is set up using [gallery link="file"] so that a Lightbox Gallery exists
	$("[data-fancybox]").fancybox({
		youtube : {
			showinfo : 0,
		}
	});

	$(".gallery a[href$='.jpg'], .gallery a[href$='.png'], .gallery a[href$='.jpeg'], .gallery a[href$='.gif']").attr('data-fancybox','gallery').fancybox();
	$('a[href$="jpg"], a[href$="png"], a[href$="jpeg"]').fancybox({
		thumbs     		: false,
		fullScreen 		: false,
		thumbs     		: false,
		loop	   		: false,
		slideShow  		: false,
		clickContent    : false,
	    buttons : [
	        'slideShow',
	        'fullScreen',
	        'thumbs',
	        //'share',
	        //'download',
	        //'zoom',
	        'close'
	    ],
	});

    $('.filter li a').click(function() {
    // fetch the class of the clicked item
    var ourClass = $(this).attr('class');
    // reset the active class on all the buttons
    $('.filter li').removeClass('active');
    // update the active state on our clicked button
    $(this).parent().addClass('active');
    if(ourClass == 'all') {
      // show all our items
      $('#projects').children('article.project').show(500);
    }
    else {
      // hide all elements that don't share ourClass
      $('#projects').children('article:not(.' + ourClass + ')').hide(500);
      // show all elements that do share ourClass
      $('#projects').children('article.' + ourClass).show(500);
    }
    return false;
	});


});

