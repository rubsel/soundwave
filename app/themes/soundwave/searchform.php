<?php ?>
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<input type="search" class="search-field" placeholder="<?php _e('Search','rby'); ?>&hellip;" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php _ex( 'Search for:', 'label', 'rby' ); ?>">
	</label>
	<button type="submit" class="search-submit" title="<?php _e('Search','rby'); ?>">
		<svg  viewBox="0 0 384 385"  xmlns="http://www.w3.org/2000/svg">
		    <path d="M381,322.7 L296.2,236.8 C310,212.7 317.2,185.9 317.2,158.9 C317.2,71.3 246,-5.68434189e-14 158.6,-5.68434189e-14 C71.2,0 0,71.3 0,158.9 C0,246.5 71.2,317.8 158.6,317.8 C186.5,317.8 214.1,310.1 238.7,295.4 L323.1,381 C325,382.9 327.7,384.1 330.4,384.1 C333.1,384.1 335.8,383 337.7,381 L381,337.2 C385,333.1 385,326.7 381,322.7 Z M158.6,61.9 C212,61.9 255.4,105.4 255.4,158.9 C255.4,212.4 212,255.9 158.6,255.9 C105.2,255.9 61.8,212.4 61.8,158.9 C61.8,105.4 105.2,61.9 158.6,61.9 Z" id="search"></path>
		</svg>
	</button>
	
</form>
