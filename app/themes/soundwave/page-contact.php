<?php get_header(); ?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'headerphoto' );?>

<div class="headerimage" style="background-image: url(<?php echo $src[0]; ?>);">
	<h1><?php the_title(); ?></h1>
</div>

<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-sm-12 col-md-10 col-md-offset-1">
			
				<?php while ( have_posts() ) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
						<div class="entry-content">
							<?php the_content(); ?>
						</div><!-- .entry-content -->
					</article><!-- #post-## -->
			
				<?php endwhile; // end of the loop. ?>
			</div><!-- close .main-content-inner -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->

<div class="goolgle-map">
	<div style="height:400px; width:100%; display:inline-block; overflow:hidden;">
		<iframe class="custom-google-map" style="position:relative; top:-50px; border:none;" src="https://www.google.com/maps/d/u/1/embed?mid=1Oo1E2fAQKWPtCh3p-bVMWUU3gWgf6u2D" width="100%" height="450"></iframe>
	</div>
</div>
<?php get_footer(); ?>
