<?php get_header(); ?>
<div class="frontpage">
<?php $images = get_field('fotoslider');

if( $images ): ?>
    <ul class="slider">
        <?php foreach( $images as $image ): ?>
            <li style="background-image: url(<?php echo $image['sizes']['headerphoto']; ?>);">
            	FOTO
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

	<div id="subheader" class="text-center">
		<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
		<h2 class="description"><?php bloginfo( 'description' ); ?></h2>
	</div>
</div>


<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-md-10 col-md-offset-1">

				<?php while ( have_posts() ) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<h2><?php the_title(); ?></h2>
					
						<div class="entry-content">
							<?php the_content(); ?>

						</div><!-- .entry-content -->
					</article><!-- #post-## -->

				<?php endwhile; // end of the loop. ?>
				
			</div><!-- close .main-content-inner -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->

<div class="projects">
	<div class="container">
		<div class="row">
			<h3><?php _e('Our projects','rby');?></h3>
			<div class="projects-wrapper">
				<?php $projects = new WP_Query('post_type=snd_portfolio&posts_per_page=4'); ?>
		
<?php if ($projects->have_posts()) : ?>
		<?php while ($projects->have_posts()) : $projects->the_post(); ?>
			<article class="project">
				<a href="<?php esc_url(the_permalink()); ?>">
					<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium_large' );?>
					<figure class="project-thumbnail" style="background-image: url(<?php echo $src[0]; ?>);">
						<h2><?php the_title(); ?></h2>
					</figure>
				</a>
			</article>
		<?php endwhile; ?>
<?php endif; ?>

<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
