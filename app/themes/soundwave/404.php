<?php get_header(); ?>
<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-sm-0 col-sm-offset-2 col-md-6 col-md-offset-3">
				

			<?php // add the class "panel" below here to wrap the content-padder in Bootstrap style ;) ?>
			<section class="content-padder error-404 not-found">
		
				<header class="page-header">
					<h2 class="page-title"><?php _e( 'Oops! Something went wrong here.', 'rby' ); ?></h2>
				</header><!-- .page-header -->
		
				<div class="page-content">
		
					<p><?php _e( 'Nothing could be found at this location. Maybe try a search?', 'rby' ); ?></p>
		
					<?php get_search_form(); ?>
		
				</div><!-- .page-content -->
		
			</section><!-- .content-padder -->

			</div><!-- close .main-content-inner -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->

<?php get_footer(); ?>