<?php 
/**
 * Add contact information
 *
 * @package TrendPress
 */

class rby_Contact_Information {
	function __construct() {
		add_action( 'admin_init', array( $this, 'add_settings' ) );
		add_action( 'admin_menu', array( $this, 'add_menu' ), 1);

		add_filter( 'option_page_capability_rby-information', array( $this, 'get_capability' ) );
	}
	
	/**
	 * Add settings fields through the Settings API
	 */
	function add_settings() {
		/**
		 * General
		 */
		add_settings_section( 'rby-general', __( 'General settings', 'rby' ), '', 'rby-information' );
		
		add_settings_field( 'rby-sitename', __( 'Sitename', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-general', array(
			'label_for' => 'rby-sitename', 
			'class' => 'regular-text', 
			'option_key' => 'blogname',
		) );
		register_setting( 'rby-information', 'rby-sitename', array( $this, 'save_site_name' ) );
		
		add_settings_field( 'rby-description', __( 'Description', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-general', array(
			'label_for' => 'rby-description',
			'class' => 'regular-text',
			'option_key' => 'blogdescription',
		) );
		register_setting( 'rby-information', 'rby-description', array( $this, 'save_site_description' ) );
		
		/**
		 * Contact
		 */
		add_settings_section( 'rby-contact', __( 'Contact', 'rby' ), '', 'rby-information' );
		
		add_settings_field( 'rby-company-name', __('Company name','rby'), array( $this, 'show_text_field' ), 'rby-information', 'rby-contact', array(
			'label_for' => 'rby-company-name',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-company-name' );
		
		add_settings_field( 'rby-address', __( 'Address', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-contact', array(
			'label_for' => 'rby-address',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-address' );
		
		add_settings_field( 'rby-postal-code', __( 'Postal code' , 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-contact', array(
			'label_for' => 'rby-postal-code',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-postal-code' );
		
		add_settings_field( 'rby-city', __( 'City', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-contact', array(
			'label_for' => 'rby-city',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-city' );
		
		add_settings_field( 'rby-country', __( 'Country', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-contact', array(
			'label_for' => 'rby-country',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-country' );
		
		add_settings_field( 'rby-email', __( 'E-mail', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-contact', array(
			'label_for' => 'rby-email',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-email' );
		
		add_settings_field( 'rby-telephone', __( 'Telephone', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-contact', array(
			'label_for' => 'rby-telephone',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-telephone' );
		add_settings_field( 'rby-mobile', __( 'Mobile', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-contact', array(
			'label_for' => 'rby-mobile',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-mobile' );
		
		add_settings_field( 'rby-fax', __( 'Fax', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-contact', array(
			'label_for' => 'rby-fax',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-fax' );
		
		/**
		 * Registration numbers & financial
		 */
		add_settings_section( 'rby-registration', __( 'Registration numbers & financial', 'rby' ), '', 'rby-information' );
		
		add_settings_field( 'rby-cc', __( 'CC No.', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-registration', array(
			'label_for' => 'rby-cc',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-cc' );
		
		add_settings_field( 'rby-vat', __( 'VAT No.', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-registration', array(
			'label_for' => 'rby-vat',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-vat' );
		
		add_settings_field( 'rby-bank', __( 'Bank name', 'rby'), array( $this, 'show_text_field' ), 'rby-information', 'rby-registration', array(
			'label_for' => 'rby-bank',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-bank' );
		
		add_settings_field( 'rby-bank-no', __( 'Bank No.', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-registration', array(
			'label_for' => 'rby-bank-no',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-bank-no' );
		
		/**
		 * Social media links
		 */
		add_settings_section( 'rby-social', __( 'Social media links', 'rby' ), '', 'rby-information' );
		
		add_settings_field( 'rby-twitter', __( 'Twitter URL', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-social', array(
			'label_for' => 'rby-twitter',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-twitter', 'rby_maybe_add_http' );
		
		add_settings_field( 'rby-facebook', __( 'Facebook URL', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-social', array(
			'label_for' => 'rby-facebook',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-facebook', 'rby_maybe_add_http' );
		
		add_settings_field( 'rby-instagram', __( 'Instagram URL', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-social', array(
			'label_for' => 'rby-instagram',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-instagram', 'rby_maybe_add_http' );
		
		add_settings_field( 'rby-linkedin', __( 'LinkedIn URL', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-social', array(
			'label_for' => 'rby-linkedin',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-linkedin', 'rby_maybe_add_http');
		
		add_settings_field( 'rby-googleplus', __( 'Google Plus URL', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-social', array(
			'label_for' => 'rby-googleplus',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-googleplus', 'rby_maybe_add_http' );
		
		add_settings_field( 'rby-youtube', __( 'YouTube URL', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-social', array(
			'label_for' => 'rby-youtube',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-youtube', 'rby_maybe_add_http' );
		
		add_settings_field( 'rby-newsletter', __( 'Newsletter / mailto link', 'rby' ), array( $this, 'show_text_field' ), 'rby-information', 'rby-social', array(
			'label_for' => 'rby-newsletter',
			'class' => 'regular-text',
		) );
		register_setting( 'rby-information', 'rby-newsletter' );
		
		add_settings_field( 'rby-rss', '', array( $this, 'show_checkbox' ), 'rby-information', 'rby-social', array(
			'label' => __( 'Show RSS feed in the social media widget', 'rby' ),
			'option_key' => 'rby-rss',
		) );
		register_setting( 'rby-information', 'rby-rss' );
	}
	
	/**
	 * Show a text field
	 *
	 * @param array $args Some additional arguments
	 */
	function show_text_field( $args ) {
		$args['option_key'] = isset( $args['option_key'] ) ? $args['option_key'] : $args['label_for'];
		?>
		<input id="<?php echo $args['label_for']; ?>" name="<?php echo $args['label_for']; ?>" value="<?php echo get_option( $args['option_key'] ); ?>" class="<?php echo $args['class']; ?>" type="text" />
		<?php
	}
	
	/**
	 * Show a checkbox
	 *
	 * @param array $args Some additional arguments
	 */
	function show_checkbox( $args ) {
		?>
		<label>
			<input name="<?php echo $args['option_key']; ?>" value="true" class="<?php echo $args['class']; ?>" type="checkbox" <?php checked( get_option( $args['option_key'] ), 'true' ); ?> />
			<?php echo $args['label']; ?>
		</label>
		<?php
	}
	
	/**
	 * Exception: Save the site name
	 */
	function save_site_name( $value ) {
		update_option( 'blogname', $value );
		return $value;
	}
	
	/**
	 * Exception: Save the site description
	 */
	function save_site_description( $value ) {
		update_option( 'blogdescription', $value );
		return $value;
	}
	
	/**
	 * Add contact information page to menu
	 */
	function add_menu() {
		add_theme_page( __( 'Contact information', 'rby' ), __( 'Contact information', 'rby' ), $this->get_capability(), 'rby-information', array( $this, 'display' ) );
	}
	
	/**
	 * Display admin page
	 */
	function display() {
		?>
		<div class="wrap">

			<div class="icon32" id="icon-themes"><br></div>

			<h2><?php _e('Contact information','rby'); ?></h2>
			
			<form action="options.php" method="post">
				<?php 
					settings_fields( 'rby-information' );
					do_settings_sections( 'rby-information' );
					submit_button(); 
				?>
			</form>

		</div>
		<?php
	}

	/**
	 * Get capability
	 */
	function get_capability() {
		return 'publish_pages';
	}
} new rby_Contact_Information;
