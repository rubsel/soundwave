<?php
// Callback function to insert 'styleselect' into the $buttons array
function rby_mce_buttons( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'rby_mce_buttons' );

/**
 * @misc Add editor styles
 */
function rby_add_editor_styles($settings) {
    $style_formats = array(
    	array(
    		'title' => __('Primary button','rby'),
    		'selector' => 'a',
    		'classes' => 'btn primary'
    	),
    	array(
    		'title' => __('Secondary button','rby'),
    		'selector' => 'a',
    		'classes' => 'btn secondary'
    	),
    	array(
    		'title' => __('More link','rby'),
    		'selector' => 'a',
    		'classes' => 'more'
    	)
    );
    $settings['style_formats'] = json_encode($style_formats);
    return $settings;
}
add_filter('tiny_mce_before_init','rby_add_editor_styles');

// Add Styling to WP Editor
add_editor_style( 'assets/css/custom-editor-style.css' );

/**
 * @Add odd/even to posts
 */
function oddeven_post_class ( $classes ) {
   global $current_class;
   $classes[] = $current_class;
   $current_class = ($current_class == 'odd') ? 'even' : 'odd';
   return $classes;
}
add_filter ( 'post_class' , 'oddeven_post_class' );
global $current_class;
$current_class = 'odd';

/*
 * @Add title to Fancybox images
 */

function rby_add_title_attachment_link($link, $id = null) {
    $id = intval( $id );
    $_post = get_post( $id );
    $post_title = esc_attr( $_post->post_excerpt );
    return str_replace('<a href', '<a title="'. $post_title .'" href', $link);
}
add_filter('wp_get_attachment_link', 'rby_add_title_attachment_link', 10, 2);


/**
 * @Move comment text field to bottom
 */
function rby_move_comment_field( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
}
add_filter( 'comment_form_fields', 'rby_move_comment_field' );

/**
 * @Add Change excerpt length
 */
 
// function rby_excerpt_length( $length ) {
// 	return 26;
// }
// add_filter( 'excerpt_length', 'rby_excerpt_length', 999 );

function rby_excerpt ( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'rby_excerpt' );

add_filter('rby_srcset_image_width', function($max_srcset_image_width, $size_array){
    return 1920;
}, 10, 2);

remove_action('wp_head', 'wp_generator');

add_action( 'customize_register', 'rby_remove_css_section', 15 );
/**
 * Remove the additional CSS section, introduced in 4.7, from the Customizer.
 * @param $wp_customize WP_Customize_Manager
 */
function rby_remove_css_section( $wp_customize ) {
    $wp_customize->remove_section( 'custom_css' );
}


?>