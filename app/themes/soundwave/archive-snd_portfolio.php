<?php get_header(); ?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'headerphoto' );?>

<div class="headerimage" style="background-image: url(<?php echo $src[0]; ?>);">
	<h1><?php _e('Portfolio','rby'); ?></h1>
</div>

<div class="filter-bar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3><?php _e('Filter our projects','rby');?></h3>
				<ul class="filter">
					<?php
						$terms = get_terms( array(
	    				'taxonomy' => 'snd_project_type',
					) );?>

				 	<li class="active"><a href="" class="all"><?php echo esc_html( _e( 'All', 'rby' )); ?></a></li>
	    			<?php foreach( $terms as $term ) : ?>
	        		<li><a href="" class="snd_project_type-<?php echo esc_html( $term->slug ); ?>"><?php echo esc_html( $term->name ); ?></a></li>
	    			<?php endforeach; wp_reset_postdata(); ?>

				</ul>
			</div>
		</div>
	</div>
</div>

<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="projects" class="main-content-inner col-md-12">
			
			<?php if( have_posts() ) : ?>

				<?php while( have_posts() ) : the_post(); ?>
			
					<article <?php post_class('project col-md-3 col-sm-6'); ?>>
						<a href="<?php esc_url(the_permalink()); ?>">

							<?php $img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium_large' );?>

							<figure style="background-image: url(<?php echo $img[0]; ?>);">
								<h2>
									<?php the_title(); ?>
								</h2>
							</figure>
						</a>
					</article>

				<?php endwhile; ?>

			<?php endif; ?>
		
			</div><!-- close .main-content-inner -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->

<?php get_footer(); ?>