<?php
    if ( post_password_required() )
    	return;
?>

<div id="comments" class="clearfix">

    <?php if ( have_comments() ) : ?>
    
        <h2 class="comments-title">
            <?php printf( _n( 'One response to &ldquo;%2$s&rdquo;', '%1$s responses to &ldquo;%2$s&rdquo;', get_comments_number(), 'rby' ), number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' ); ?>
        </h2>
        
        <ol class="commentlist">
            <?php wp_list_comments(
            	array(
				'walker'            => null,
				'max_depth'         => '',
				'style'             => 'ul',
				'callback'          => null,
				'end-callback'      => null,
				'type'              => 'all',
				'reply_text'        => '<svg viewBox="0 0 384 288" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <path d="M384,288 C384,288 347.2,80 160,80 L160,0 L0,144 L160,278.4 L160,186.1 C261.6,186.1 331,195 384,288 Z" id="Shape"></path>
                                        </svg>'.__('Reply','rby').'',
				'page'              => '',
				'per_page'          => '',
				'avatar_size'       => 60,
				'reverse_top_level' => null,
				'reverse_children'  => '',
				'format'            => 'html5',
				'short_ping'        => false
			));
            ?>
        </ol>
        
    <?php endif; ?>
    
    <?php if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
    
    	<p class="no-comments">
    		<?php _e( 'Comments are closed', 'rby' ); ?>.
    	</p>
    	
    <?php endif; ?>
    
    <?php comment_form(); ?>
    
</div>