<?php get_header(); ?>
<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'headerphoto' );?>

<div class="headerimage" style="background-image: url(<?php echo $src[0]; ?>);">
	<h1><?php the_title(); ?></h1>
</div>

<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-md-8 col-md-offset-2">

		<article id="portfolio-single">
		
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
				<div>
					<?php the_content(); ?>


				</div>


				<p>
					<a class="back-link" href="<?php echo esc_url( get_post_type_archive_link('snd_portfolio')); ?>">
						<?php _e( 'Back to the overview', 'rby' ); ?>
					</a>
				</p>



			<?php endwhile; endif; ?>


			</article>

	</div><!-- close .main-content-inner -->

			</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
<?php get_footer(); ?>